/*
¿Qué axioma se necesita para inferir el hecho
Femenino(Laura) dados los 
hechos Masculino(Jorge) y Esposo(Jorge, Laura).
*/
masculino(jorge).
casado(jorge,laura).

esposo(jorge, laura):- masculino(jorge),casado(jorge,laura).

femenino(laura):- casado(jorge,laura), esposo(jorge,laura)
