/*
 ¿Qué axioma se necesita para inferir el hecho Femenino(Laura) dados los hechos Masculino(Jorge) y Esposo(Jorge, Laura).
*/

marido(jorge,laura):- masculino(jorge), esposo(jorge,laura).

mujer(laura, jorge):- femenino(laura), esposo(laura,jorge).
