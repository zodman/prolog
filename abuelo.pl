/*

abuel@s (x,y)
*/

padre(andres, andresP).
padre(mauricio, andresP).
padre(andresP,andresA).

madre(andres,merly).
madre(mauricio, merly).
madre(merly, elide).

abuelo(X,Y):- padre(X,P), padre(P,Y).
%abuelo(andres,andresA)
%

abuela(X,Y):- madre(X,P), madre(P,Y).

abuel_s(N,X,Y):- abuelo(N,X), abuela(N,Y).
