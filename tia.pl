/*
Escribe una regla sobre la definición de una tía materna.
*/

madre(paty, luz).
madre(paty, lucha).
madre(lucha, lupita).

hermana(X,Y):- madre(P,X), madre(P,Y), X\=Y.


/* Entonces, ¿cualquier pariente es tu tía?*/
%tia(X,Y):- mujer(Y), pariente(X,Y).
tia(X,Y):- hermana(X,Y), madre(Y,W).

