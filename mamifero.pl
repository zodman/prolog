
/*
 Escribe la representación lógica para las siguientes sentencias.
 a) Los caballos, las vacas y los cerdos son mamíferos.
 b) El descendiente de un caballo es un caballo.
 c) Cada mamífero tiene un padre.
 d) Belleza Negra es un caballo.
 e) Silver es padre de Belleza Negra.
 f) ¿Es Silver un mamífero?
 */

mamifero(X):- caballo(X).
caballo(H):- descendiente(P,H), caballo(P).
padre(P,H):- mamifero(H).
caballo(bellezanegra).
padre(silver, bellezanegra).
/*que es decendiente */
decendiente(P,H):- padre(P,H).
 
