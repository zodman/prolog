/*
Escribe una regla para: Algunos estudiantes estudian francés en la primavera del 2012
*/

estudiante(persona_uno).
estudiante(persona_dos).
frances(persona_uno).
frances(persona_dos).

primavera(A):- A=2012.
estudia_frances(X,A):- estudiante(X),frances(X), primavera(A).


